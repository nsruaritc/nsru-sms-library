# README

## รายการที่ต้องทำหลังจากที่ FORK ไปแล้ว
1. แก้ไขไฟล์ composer.json
   1. name : ชื่อของ Library
   2. description : คำอธิบายเกี่ยวกับ Library
   3. psr-4 : กำหนดให้ตรงกับไลบราลี่เรา
   4. authors : ผู้พัฒนา

## การทดสอบ
ทดสอบการ Push
```
php ./tests/sample.php
```
<?php
namespace Nsru\Sms;

use Exception;

class Sms
{
    private $accessToken;

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function send($toPhoneNumber, $message)
    {
        try {
            $url = "https://api.nsru.ac.th/sms/api/v1/send";
            $data = [
                'phone_number'  => $toPhoneNumber,
                'message'       => $message
            ];
            $bearerToken = "Bearer {$this->accessToken}";
            $data = http_build_query($data);
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: ' . $bearerToken
            ]);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            $response = curl_exec($ch);
    
            if(curl_errno($ch)) {
                throw new Exception(curl_error($ch));
            } else {
                if($response) {
                    $response = json_decode($response);
                    if($response->is_success) {
                        return true;
                    } else {
                        throw new Exception($response->message);
                    }
                } else {
                    throw new Exception('No response from server');
                }
            }
        } catch( Exception $e ) {
            return false;
        }
    }

    public function credits()
    {
        try {
            $url = "https://api.nsru.ac.th/sms/api/v1/info";
            $bearerToken = "Bearer {$this->accessToken}";
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: ' . $bearerToken
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            $response = curl_exec($ch);
    
            if(curl_errno($ch)) {
                throw new Exception(curl_error($ch));
            } else {
                if($response) {
                    $response = json_decode($response);
                    if($response->is_success) {
                        return $response->data->credits;
                    } else throw new Exception($response->message);
                } else throw new Exception('No response from server');
            }
        } catch( Exception $e ) {
            return false;
        }
    }

    public function info()
    {
        try {
            $url = "https://api.nsru.ac.th/sms/api/v1/info";
            $bearerToken = "Bearer {$this->accessToken}";
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: ' . $bearerToken
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            $response = curl_exec($ch);
    
            if(curl_errno($ch)) {
                throw new Exception(curl_error($ch));
            } else {
                if($response) {
                    $response = json_decode($response);
                    if($response->is_success) {
                        return $response->data;
                    } else throw new Exception($response->message);
                } else throw new Exception('No response from server');
            }
        } catch( Exception $e ) {
            return false;
        }
    }
}